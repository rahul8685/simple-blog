import functools
import re

from flask import Blueprint, g, request, session
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

bp = Blueprint("auth", __name__, url_prefix="/auth")


def login_required(view):
    """View decorator that redirects anonymous users to the login page."""

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return "Please login first", 401, ()

        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    """If a user id is stored in the session, load the user object from
    the database into ``g.user``."""
    user_id = session.get("user_id")

    if user_id is None:
        g.user = None
    else:
        g.user = (
            get_db().execute("SELECT * FROM user WHERE id = ?", (user_id,)).fetchone()
        )


@bp.route("/register", methods=("POST",))
def register():
    """Register a new user.
    Validates that the username is not already taken. Hashes the
    password for security.
    """
    username = request.json.get("username")
    password = request.json.get("password")
    db = get_db()
    error = None

    if not username:
        error = "Username is required."
    elif not password:
        error = "Password is required."
    elif (
        db.execute("SELECT id FROM user WHERE username = ?", (username,)).fetchone()
        is not None
    ):
        error = f"User {username} is already registered."

    if not re.match(r"^[a-zA-Z0-9\-_]+$", username):
        error = f"Username can only contain alphanumeric, underscore (_) and dash (-) characters"

    if error:
        return error, 400, ()

    # the name is available, store it in the database and go to
    # the login page
    db.execute(
        "INSERT INTO user (username, password) VALUES (?, ?)",
        (username, generate_password_hash(password)),
    )
    db.commit()
    return "success"


@bp.route("/login", methods=("POST",))
def login():
    """Log in a registered user by adding the user id to the session."""

    username = request.json.get("username")
    password = request.json.get("password")
    db = get_db()
    error = None

    if not username:
        error = "Username is required."
    elif not password:
        error = "Password is required."

    if error:
        return error, 400, ()

    user = db.execute("SELECT * FROM user WHERE username = ?", (username,)).fetchone()

    if user is None:
        error = "Incorrect username."
    elif not check_password_hash(user["password"], password):
        error = "Incorrect password."

    if error:
        return error, 401, ()

    if error is None:
        # store the user id in a new session and return to the index
        session.clear()
        session["user_id"] = user["id"]
        return "success"


@bp.route("/logout", methods=("POST",))
@login_required
def logout():
    """Clear the current session, including the stored user id."""
    session.clear()
    session["user_id"] = None
    return "success"
