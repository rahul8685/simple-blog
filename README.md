# Simple Blog

This is a simple CRUD api for a blogging app written in flask. The app accepts only json input and responds with json data. The app provides the following endpoints:

### POST `/auth/register`
Register a new user to create blogs

##### example request data:
```json
{
    "password": "admin",
    "username": "admin"
}
```

##### Possible responses:
* `status: 200` - Request successful
* `status: 400` - Invalid request
* `status: 500` - Internal server error 

### POST `/auth/login`
login to app with given username and password

##### example request data:
```json
{
    "password": "admin",
    "username": "admin"
}
```

##### Possible responses:
* `status: 200` - Request successful
* `status: 400` - Invalid request
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

### POST `/auth/logout`
logout of the app

##### Possible responses:
* `status: 200` - Request successful
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

### POST `/create`
Create a new blog post with given title and optional body

##### example request data:
```json
{
    "title": "A new blog",
    "body": "This is the body of a new blog that I am going to write"
}
```

##### Possible responses:
* `status: 200` - Request successful
* `status: 400` - Invalid request
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

### POST `/<blog_id>/update`
Update a previous blog post with given title and optional body

##### example request data:
```json
{
    "title": "A new updated blog",
    "body": "Updated body of the blog post"
}
```

##### Possible responses:
* `status: 200` - Request successful
* `status: 400` - Invalid request
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

### GET `/`
Get all blog posts

##### Possible responses:
* `status: 200` - Request successful
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

##### Response on success:
```json
[
    {
        "author_id": 1,
        "body": "Updated body of the blog post",
        "created": "Mon, 27 Aug 2018 07:04:43 GMT",
        "id": 2,
        "title": "A new blog",
        "username": "admin"
    }
]
```

### GET `/<blog_id>`
Get one blog post given its blog_id

##### Possible responses:
* `status: 200` - Request successful
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

##### Response on success:
```json
{
    "author_id": 1,
    "body": "Updated body of the blog post",
    "created": "Mon, 27 Aug 2018 07:04:43 GMT",
    "id": 2,
    "title": "A new blog",
    "username": "admin"
}
```

### DELETE `/<blog_id>`
Delete a blog post given its blog_id

##### Possible responses:
* `status: 200` - Request successful
* `status: 401` - Invalid credentials
* `status: 500` - Internal server error

# Setting up the app for development

#### Clone app
```csh
# git clone /home/rahul/workspace/CRUD-Flask
# cd simple-blog
```

#### Set up python virtual env
```csh
# python3 -m venv ./venv
# source venv/bin/activate
```

#### Install dependencies
```csh
# pip install -r requirements.txt
```

#### Start the app
```csh
# FLASK_APP=flaskr FLASK_ENV=development flask run
```

# The Assignment
Your TASK is to create test cases for the entire app using `HTTP requests to the API endpoints` in Python.

* You are free to use any python lib you deem useful.
* You can structure/organize your test cases any way you like.
* Try and complete the assignment in the given time. It's totally fine if it's not complete. We would love to see the assignment anyway.
* Call or mail us if you have any trouble understanding anything.
